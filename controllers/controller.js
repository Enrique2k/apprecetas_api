//File: controllers/tvshows.js
var mongoose = require('mongoose');
var Ingrediente = require('../models/ingrediente');
var Paso = require('../models/paso');
var Receta = require('../models/receta');
var User = require('../models/user');
var Like = require('../models/like');
var Plan = require('../models/plan');
var Fav = require('../models/fav');
var path = require('path');
var fs = require('fs-extra');

//GET ALL
//Recetas
exports.findAllRecetas = function(req, res) {
    Receta.find(function(err, result) {
    if(err) res.status(500).send(err.message);
        console.log('GET /recetas')
        res.status(200).jsonp(result);
    });
};

//Users
exports.findAllUsers = function(req, res) {
    User.find(function(err, users) {
    if(err) res.status(500).send(err.message);
        console.log('GET /users')
        res.status(200).jsonp(users);
    });
};

//Validate if user exist
exports.findUser = function(req, res) {
    User.find({ user_name : req.params.id },function(err, result) {

    if(err) res.status(500).send(err.message);
        console.log('GET /receta')
        res.status(200).jsonp(result);
    });
};
//GET ALL PASOS OF AN ID
exports.findAllPasosReceta = function(req, res) {
    Paso.find( { id_receta: req.params.id },function(err, result) {
    if(err) res.status(500).send(err.message);
        console.log('GET /pasos of a receta')
        res.status(200).jsonp(result);
    });
};

//find all likes of an user
exports.findAllLikesUser = function(req, res) {

    Like.find({'user_name':req.params.user} ,function(err, result) {
            if(err) res.status(500).send(err.message);
            console.log('GET /likes ' + req.params.user);
            res.status(200).jsonp(result);
        }
    )
    //.skip(req.params.id).limit(1);
};

exports.findAllPlans = function(req, res) {

    Plan.aggregate([
        {
          $match : {
            "user_name": req.params.user
          }
        },
       {
          $lookup:
             {
               from: "recetas",
               localField : "id_receta",
               foreignField : "id_receta",
               as: "receta"
             }
        },
        {
            $unwind:
              {
                path: "$receta",
              }
        }
    ] , function(err, result) {
        if(err) res.status(500).send(err.message);
        console.log('GET /Favs ' + req.params.user);
        res.status(200).jsonp(result);
    })
};

exports.findAllFavs = function(req, res) {

    Fav.aggregate([
        {
          $match : {
            "user_name": req.params.user
          }
        },
       {
          $lookup:
             {
               from: "recetas",
               localField : "id_receta",
               foreignField : "id_receta",
               as: "receta"
             }
        },
        {
            $unwind:
              {
                path: "$receta",
                
              }
        }
    ] , function(err, result) {
        if(err) res.status(500).send(err.message);
        console.log('GET /Favs ' + req.params.user);
        res.status(200).jsonp(result);
    })
};
//GET ALL Ingredientes OF AN ID
exports.findAllIngreReceta = function(req, res) {
    Ingrediente.find( { id_receta : req.params.id },function(err, result) {
    if(err) res.status(500).send(err.message);
        console.log('GET /ingres of a receta')
        res.status(200).jsonp(result);
    });
};

//get all recetas 5 in 5
exports.findAllRecetas2 = function(req, res) {
    Receta.find(function(err, result) {
    if(err) res.status(500).send(err.message);
        console.log('GET /recetas limit')
        res.status(200).jsonp(result);
    }).skip(parseInt(req.params.id)).limit(5).sort({"id_receta":-1});
};

//filtrar recetas por nombre
exports.findRecetasByTitulo = function(req, res) {

    Receta.find( { titulo : { $regex: '.*' + req.params.id + '.*', $options: 'i' } },function(err, result) {
        if(err) res.status(500).send(err.message);
            console.log('GET /recetas by titulo')
            res.status(200).jsonp(result);
    });
};

//filtrar por tipo de alimento
exports.findRecetasByAlimento = function(req, res) {
    Receta.find( { tipo_alimento : req.params.id },function(err, result) {
    if(err) res.status(500).send(err.message);
        console.log('GET /recetas by ta')
        res.status(200).jsonp(result);
    });
};

//filtrar por tags
exports.findRecetasByTag = function(req, res) {

    Receta.find( { esp_tag : req.params.id },function(err, result) {
        if(err) res.status(500).send(err.message);
            console.log('GET /recetas by ta')
            res.status(200).jsonp(result);
        });
    
    
};

//update foto with id
exports.updateReceta = function(req, res) {
    Receta.findById(req.params.id, async function(err, receta) {
        await fs.unlink(path.resolve(receta.url_foto));
        receta.url_foto = req.file.path;
        console.log('Put /receta/foto' + req.params.id);
        receta.save(function(err) {
            if(err) return res.status(500).send(err.message);
            res.status(200).jsonp(receta);
          });
    });
};

//update like with id
exports.updateLike = function(req, res) {
    Receta.findById(req.params.id, function(err, receta) {
        var num = parseInt(req.params.like);

        receta.likes = receta.likes + num;

        console.log('update /receta/like' + req.params.id);
        receta.save(function(err) {
            if(err) return res.status(500).send(err.message);
            res.status(200).jsonp(receta);
          });

    });
};

//update plan with id user day
exports.updatePlan = function(req, res) {
    Plan.findOne( {"user_name" : req.params.user , "id_receta": req.params.id}, function(err, plan) {
       
        plan.day = req.params.day;

        console.log('update /plan/day' + req.params.id);
        plan.save(function(err) {
            if(err) return res.status(500).send(err.message);
            res.status(200).jsonp(plan);
          });

    });
};

exports.createReceta = function(req, res) {
    const { tipo_alimento, id_receta , titulo , descripcion , dificultat , tiempo ,kcal , tipo_plato , esp_tag} = req.body;

    const newReceta = {
        titulo:  titulo,
        id_receta:     id_receta,
        url_foto:      req.file.path,
        descripcion:   descripcion,
        dificultat:    dificultat,
        tiempo:        tiempo,
        kcal:          kcal,
        tipo_plato:    tipo_plato,
        likes:         0,
        tipo_alimento: tipo_alimento,
        esp_tag:       esp_tag
    }
    const receta = new Receta(newReceta);

    console.log('Put new /receta');

    receta.save(function(err) {

        if(err) return res.status(500).send(err.message);
        res.status(200).jsonp(receta);

    });
    
};


exports.createIngrediente = function(req, res) {
    const { id_receta, nombre } = req.body;
  
    const newingrediente = {
        id_receta: id_receta,
        nombre:    nombre,
    }

    const ingrediente = new Ingrediente(newingrediente);
    console.log('Put new /ingrediente');
    ingrediente.save(function(err) {

        if(err) return res.status(500).send(err.message);
        res.status(200).jsonp(ingrediente);

    });
};

exports.createUser = function(req, res) {
    const { nombre, user_name , user_password } = req.body;
  
    const newUser = {
        nombre:        nombre,
        user_name:     user_name,
        user_password: user_password,
    }

    const user = new User(newUser);
    console.log('Put new /user');
    user.save(function(err) {
        if(err) return res.status(500).send(err.message);
        res.status(200).jsonp(user);

    });
};

exports.createLike = function(req, res) {
    const { user_name , id_receta } = req.body;
    
    const newLike = {
        user_name: user_name,
        id_receta: id_receta,
    }

    const like = new Like(newLike);

    console.log('Put new /like');
    like.save(function(err) {
        if(err) return res.status(500).send(err.message);
        res.status(200).jsonp(like);

    });
};

exports.createPaso = function(req, res) {
    const { id_receta, numPaso , descripcion} = req.body;
    const newPaso = {
        id_receta: id_receta,
        numPaso:    numPaso,
        descripcion: descripcion,
        foto: req.file.path
    }

    const paso = new Paso(newPaso);
    console.log('Put new /paso');
    paso.save(function(err) {

        if(err) return res.status(500).send(err.message);
        res.status(200).jsonp(paso);

    });
};

exports.createFav = function(req, res) {

    const { user_name , id_receta } = req.body;
    
    const newFav = {
        user_name: user_name,
        id_receta: id_receta,
    }

    const fav = new Fav(newFav);

    console.log('Put new /Fav');
    fav.save(function(err) {
        if(err) return res.status(500).send(err.message);
        res.status(200).jsonp(fav);

    });
};

exports.createPlan = function(req, res) {
    const { user_name , id_receta , day } = req.body;
    
    const newPlan = {
        user_name: user_name,
        id_receta: id_receta,
        day:       day,
    }

    const plan = new Plan(newPlan);

    console.log('Put new /Plan');
    plan.save(function(err) {
        if(err) return res.status(500).send(err.message);
        res.status(200).jsonp(plan);

    });
};

exports.deleteReceta = function(req , res){

    Receta.findByIdAndRemove(req.params.id, function(err, receta) {
        if(receta) {
            borrarPasos(receta.id_receta);
            borrarIngredientes(receta.id_receta);
            fs.unlink(path.resolve(receta.url_foto));
        }
        console.log('Delete /receta/' + req.params.id);
        
    });
}

exports.deleteLike = function(req , res){

    Like.deleteOne({ 'user_name': req.params.user , 'id_receta': req.params.id }, function (err) {
        if (err) return handleError(err);
        
      });
}

exports.deletePlan = function(req , res){

    Plan.deleteOne({ 'user_name': req.params.user , 'id_receta': req.params.id }, function (err) {
        if (err) return handleError(err);
        
      });
}

exports.deleteFav = function(req , res){

    Fav.deleteOne({ 'user_name': req.params.user , 'id_receta': req.params.id }, function (err) {
        if (err) return handleError(err);
        
      });
}

exports.deleteAllPlans = function(req , res){

    Plan.deleteMany({ 'user_name': req.params.user }, function (err) {
        if (err) return handleError(err);
      });
}

exports.deleteAllFavs = function(req , res){

    Fav.deleteMany({ 'user_name': req.params.user }, function (err) {
        if (err) return handleError(err);
        
      });
}

//GET one with id 
exports.findRecetaById = function(req, res) {
    Receta.findById(req.params.id, function(err, result) {
        if(err) res.status(500).send(err.message);
        console.log('GET /receta/' + req.params.id);
        res.status(200).jsonp(result);
    });
};

async function borrarPasos(id) {
    const pasos = await  Paso.find({id_receta: id });
    
    for(var x = 0; x < pasos.length; x++){
        fs.unlink(path.resolve(pasos[x].foto));
    }
    Paso.deleteMany({id_receta: id }, function(err, paso) {
        if(err) res.status(500).send(err.message);
        console.log('Delete /Pasos/' + id);
    });
}

function borrarIngredientes(id){
    Ingrediente.deleteMany({id_receta: id }, function(err, result) {
        if(err) res.status(500).send(err.message);
        console.log('Delete /Ingredientes/' + id);
    });
}
