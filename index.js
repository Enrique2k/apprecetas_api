var express = require("express");
var app = express();
var mongoose = require('mongoose');
const cors = require('cors');
const multer = require('./libs/multer')
app.use(cors());
var apiCtrl = require('./controllers/controller');


//https://www.youtube.com/watch?v=OMBwyCNmoPY&ab_channel=FaztCode
//https://www.youtube.com/watch?v=4MEsROoq5Qw&ab_channel=FaztCode

var router = express.Router();

//update foto
router.route('/receta/foto/:id').get(multer.single('image'),apiCtrl.updateReceta);
//update like
router.route('/receta/like/:id/:like').get(multer.single('image'),apiCtrl.updateLike);

//new item / get all recetas / get all users
router.route('/recetas/').get(apiCtrl.findAllRecetas).post(multer.single('image'),apiCtrl.createReceta);
router.route('/pasos/').post(multer.single('image'),apiCtrl.createPaso);
router.route('/ingredientes/').post(multer.single('image'),apiCtrl.createIngrediente);
router.route('/users/').get(apiCtrl.findAllUsers).post(multer.single('image'),apiCtrl.createUser);
router.route('/likes/').post(multer.single('image'),apiCtrl.createLike);
router.route('/plans/').post(multer.single('image'),apiCtrl.createPlan);
router.route('/favs/').post(multer.single('image'),apiCtrl.createFav);


//find all likes of an user
router.route('/likes/:user').get(apiCtrl.findAllLikesUser);
//get all recetas favs of an user
router.route('/favs/:user').get(apiCtrl.findAllFavs);
//get all recetas plns of an user
router.route('/plans/:user').get(apiCtrl.findAllPlans);

//get recetas in 5 / 5
router.route('/recetas/:id').get(apiCtrl.findAllRecetas2);
//get all recetas liked from an user

//filtrar recetas por nombre
router.route('/receta/titulo/:id').get(apiCtrl.findRecetasByTitulo);
//filtrar por tipo de alimento
router.route('/receta/alimento/:id').get(apiCtrl.findRecetasByAlimento);
//filtrar por tags
router.route('/receta/tag/:id').get(apiCtrl.findRecetasByTag);

//get all * of a receta
router.route('/pasos/:id').get(apiCtrl.findAllPasosReceta);
router.route('/ingredientes/:id').get(apiCtrl.findAllIngreReceta);

//validate User
router.route('/users/:id').get(apiCtrl.findUser);

//delete
router.route('/receta/:id').get(apiCtrl.findRecetaById).delete(apiCtrl.deleteReceta);
router.route('/like/:user/:id').delete(apiCtrl.deleteLike);
router.route('/plan/:user/:id').delete(apiCtrl.deletePlan);
router.route('/fav/:user/:id').delete(apiCtrl.deleteFav);
//delete all favs / all plans
router.route('/plan/:user/').delete(apiCtrl.deleteAllPlans);
router.route('/fav/:user/').delete(apiCtrl.deleteAllFavs);
//modificar dia de un plan
router.route('/plan/:id/:user/:day').get(apiCtrl.updatePlan);

app.use('/api',router);

const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null;

mongoose.connect('mongodb://enrique-moran-7e3:ITB2019017@mongodb-enrique-moran-7e3.alwaysdata.net:27017/enrique-moran-7e3_batabase', {useNewUrlParser: true}, function(err, res) {
  if(err) {
    console.log( "Failed to connect to the database" + err);
  }else{
    console.log("All Works");
  }
  app.listen(PORT , IP, function() {
    console.log("Node server running on http://localhost:3000");
  });
});
