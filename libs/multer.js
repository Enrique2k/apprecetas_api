var multer = require('multer');

var path = require('path');

const storage = multer.diskStorage({
    destination: 'uploads',
    filename: (req, file,cb)=>{
        cb(null, new Date().getTime() + path.extname(file.originalname));
    }
});

module.exports =  multer({storage});