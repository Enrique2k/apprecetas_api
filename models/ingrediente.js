var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var ingrediente = new Schema({
    
    id_receta: { type: String } ,
    nombre:    { type: String }
      
});

module.exports = mongoose.model('Ingrediente' , ingrediente);