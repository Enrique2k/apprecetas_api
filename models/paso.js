var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var paso = new Schema({
    
    id_receta: { type: String } ,
    numPaso: { type: Number },
    descripcion:  [[]],
    foto: { type: String } 
});

module.exports = mongoose.model('Paso' , paso);