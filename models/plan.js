var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var userPlan = new Schema({
  
  user_name: { type: String },
  id_receta: { type: String },
  day:       { type: String },
});


module.exports = mongoose.model('Plan' , userPlan);