var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var receta = new Schema({
  titulo:        { type: String },
  id_receta:     { type: String },
  url_foto:      { type: String },
  descripcion:   { type: String },
  dificultat:    { type: String },
  tiempo:        { type: String },
  kcal:          { type: String },
  tipo_plato:    { type: String },
  likes:         { type: Number },
  tipo_alimento: [{ type: String }],
  esp_tag:       [{ type: String }]
});


module.exports = mongoose.model('Receta' , receta);
