var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var user = new Schema({
  nombre:        { type: String },
  user_name:     { type: String },
  user_password: { type: String },
});


module.exports = mongoose.model('User' , user);